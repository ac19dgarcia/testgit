package model;

public class CalculadoraModel {

    public double suma(double op1, double op2) {
        return op1 + op2;
    }

    public double resta(double op1, double op2) {
        return op1 - op2;
    }

    public double multiplicacio(double op1, double op2) {
        return op1 * op2;
    }

    public double divisio(double op1, double op2) {
        return op1 / op2;
    }
}
