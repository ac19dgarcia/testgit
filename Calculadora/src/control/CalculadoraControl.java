package control;

import model.CalculadoraModel;
import vista.CalculadoraVista;

public class CalculadoraControl {

    private CalculadoraVista vista;
    private CalculadoraModel model;

    public CalculadoraControl(CalculadoraVista vista, CalculadoraModel model) {
        this.vista = vista;
        this.model = model;
    }

    public void iniciar() {
        String opcio;
        double op1, op2;

        while (!(opcio = vista.demanarOpcioMenu()).equals("5")) {

            op1 = vista.demanarOperand("Operand 1 -> ");
            op2 = vista.demanarOperand("Operand 2 -> ");

            switch (opcio) {
                case "1":
                    vista.mostrarResultat(model.suma(op1, op2));
                    break;
                case "2":
                    vista.mostrarResultat(model.resta(op1, op2));
                    break;
                case "3":
                    vista.mostrarResultat(model.multiplicacio(op1, op2));
                    break;
                case "4":
                    if (op2 == 0) {
                        vista.mostrarMissatge("No es pot dividir per 0!");
                    } else {
                        vista.mostrarResultat(model.divisio(op1, op2));
                    }
                    break;
            }
        }

    }

}
