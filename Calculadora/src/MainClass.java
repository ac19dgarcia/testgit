
import control.CalculadoraControl;
import model.CalculadoraModel;
import vista.CalculadoraVista;


public class MainClass {

  
    public static void main(String[] args) {
        
        new CalculadoraControl(new CalculadoraVista(), new CalculadoraModel()).iniciar();
      
    }

}
