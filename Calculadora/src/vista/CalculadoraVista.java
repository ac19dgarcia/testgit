package vista;

import java.util.Scanner;

public class CalculadoraVista {

    Scanner in = new Scanner(System.in);

    public String demanarOpcioMenu() {
        String opcio;
        System.out.println("1. Suma"
                + "\n2. Resta"
                + "\n3. Multiplicació"
                + "\n4. Divisió"
                + "\n5. Sortir");
        while(!in.hasNext("[1-5]")){
            System.out.println("Opció incorrecta"); 
            in.nextLine();
        }        
        return in.nextLine();
    }

    public double demanarOperand(String missatge) {
        System.out.println(missatge);

        while (!in.hasNextDouble()) {
            System.out.println("Valor incorrecte");
            in.nextLine();
        }
        return Double.parseDouble(in.nextLine().replace(',', '.'));
    }

    public void mostrarResultat(double resultat) {
        System.out.printf("Resultat -> %.2f%n", resultat);
    }

    public void mostrarMissatge(String missatge) {
        System.out.println(missatge);
    }
}
